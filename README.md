# Snake

A spin on the classic snake game, complete with a retro re-design and adversaries that track you down.

## Use

Use the arrow keys to move.

Try to eat as much food (yellow) as possible to grow.

If you run over your own tail, your tail will be chopped off.

The Eagle (white) will hunt you down, unless distracted by nearby food. You can use this to your advantage.

## Development

Written in Vanilla JavaScript.


## Author

Noah Tigner

nzt@cs.uoregon.edu